Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: RANN
Upstream-Contact: Gregory Jefferis <jefferis@gmail.com>
Source: https://cran.r-project.org/package=RANN

Files: *
Copyright: 1997-2019 Sunil Arya and David Mount (for ANN),
                Samuel E. Kemp
                Gregory Jefferis <jefferis@gmail.com>
License: GPL-3+

Files:
 src/kd_fix_rad_search.h
 src/ANN.cpp
 src/kd_dump.cpp
 src/bd_tree.h
 src/kd_tree.h
 src/ANN/ANNx.h
 src/ANN/ANNperf.h
 src/ANN/ANN.h
 src/kd_split.h
 src/pr_queue.h
 src/kd_split.cpp
 src/kd_search.h
 src/kd_fix_rad_search.cpp
 src/bd_fix_rad_search.cpp
 src/bd_tree.cpp
 src/pr_queue_k.h
 src/kd_util.h
 src/kd_pr_search.h
 src/bd_search.cpp
 src/kd_pr_search.cpp
 src/kd_tree.cpp
 src/bd_pr_search.cpp
 src/kd_search.cpp
 src/kd_util.cpp
 src/brute.cpp
Copyright: 1997-2005 Sunil Arya
                     David Mount
                     University of Maryland
License: LGPL-2

Files: debian/*
Copyright: 2019 Steffen Moeller <moeller@debian.org>
License: GPL-3+

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems you can find the full text of the GNU General Public
 License version 3 at /usr/share/common-licenses/GPL-3.

License: LGPL-2
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either version
 2.1 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser
 General Public License can be found in "/usr/share/common-licenses/LGPL-2".
 
